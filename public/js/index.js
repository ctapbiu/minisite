jQuery(document).ready(function() {
  jQuery('.like').on('click', function() {
    var el = this;
    var oldClass = jQuery(el).hasClass('liked') ? 'liked' : 'not-liked';
    var newClass = jQuery(el).hasClass('liked') ? 'not-liked' : 'liked';

    jQuery(el)
      .removeAttr(oldClass)
      .find('i')
        .removeClass('fa-thumbs-o-up')
        .addClass('fa-spinner fa-spin')

    jQuery.ajax({
      url: likeUrl,
      type: 'POST',
      dataType: 'json',
      data: {status: jQuery(this).data('status'), id: jQuery(this).data('user')},
      success: function (data) {
        if ('undefined'==typeof(data.success)) {
          if ('undefined'!=typeof(data.msg)) {
            jQuery('#alerts').html('<div class="alert alert-error fade in">'+data.msg+'</div>');
          }

          jQuery(el)
          .attr('class', oldClass)
          .find('i')
          .removeClass('fa-spinner fa-spin')
          .addClass('fa-thumbs-o-up')
          return false;
        }

        if ('undefined'!=typeof(data.remainLikes)) {
          jQuery('#remain-likes').text(data.remainLikes);
        }

        if ('undefined'!=typeof(data.msg)) {
          jQuery('#alerts').html('<div class="alert alert-success fade in">'+data.msg+'</div>');
        }

        jQuery(el)
          .attr('class', newClass)
          .data('status', newClass)
          .find('i')
            .removeClass('fa-spinner fa-spin')
            .addClass('fa-thumbs-o-up')
      },
      error: function (data) {
        if ('undefined'!=typeof(data.msg)) {
          jQuery('#alerts').html('<div class="alert alert-error fade in">'+data.msg+'</div>');
        }

        jQuery(el)
          .attr('class', oldClass)
          .find('i')
            .removeClass('fa-spinner fa-spin')
            .addClass('fa-thumbs-o-up')
      }
    });

    return false;
  });



});