<?php
namespace Application\Form;

use Zend\Form\Form;
use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Doctrine\ORM\EntityManager;

class SignUpForm extends Form {

  public function __construct(EntityManager $em, $name = null) {
    parent::__construct('createUser');
    $this->setAttribute('method', 'post');
    $this->setInputFilter(new \Application\Form\SignUpInputFilter($em));

    $this->add(array(
      'name' => 'name',
      'type' => 'Text',
      'options' => array(
        'label' => 'Name',
      ),
      'options' => array(
        'min' => 3,
        'max' => 25,
      ),
    ));

    $this->add(array(
      'name' => 'email',
      'type' => 'Text',
      'options' => array(
        'label' => 'Email',
      ),
      'options' => array(
      ),
    ));

    $this->add(array(
      'name' => 'password',
      'type' => 'Password',
      'options' => array(
        'label' => 'Password',
      ),
      'options' => array(
        'min' => 6,
      ),
    ));

    $this->add(array(
      'name' => 'submit',
      'type' => 'Submit',
    ));
  }
}