<?php

namespace Application\Form;

use Zend\Form\Form;
use Zend\InputFilter\InputFilter;

class SearchInputFilter extends InputFilter
{
    public function __construct()
    {
        $this->add(array(
            'name' => 'search',
            'required' => true,
            'validators' => array(
            ),
            'filters' => array(
                array('name' => 'StripTags'),
                array('name' => 'StringTrim'),
            ),

        ));
    }
}