<?php
namespace Application\Form;

use Zend\Form\Form;
use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;

class SearchForm extends Form {

  public function __construct($name = null) {
    parent::__construct('searchUsers');
    $this->setAttribute('method', 'get');
    $this->setInputFilter(new \Application\Form\SearchInputFilter());

    $this->add(array(
      'name' => 'search',
      'type' => 'Text',
      'options' => array(
        'label' => 'Search',
      ),
      'options' => array(
      ),
    ));

    $this->add(array(
      'name' => 'submit',
      'type' => 'Submit',
    ));
  }
}