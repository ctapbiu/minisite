<?php

namespace Application\Form;

use Zend\Form\Form;
use Zend\InputFilter\InputFilter;
use Doctrine\ORM\EntityManager;

class SignUpInputFilter extends InputFilter
{
    public function __construct(EntityManager $em)
    {
        $this->add(array(
            'name' => 'name',
            'required' => true,
            'validators' => array(
                array(
                    'name' => 'StringLength',
                    'options' => array(
                        'min' => 3,
                        'max' => 25,
                    ),
                ),
            ),
            'filters' => array(
                array('name' => 'StripTags'),
                array('name' => 'StringTrim'),
            ),

        ));

        $this->add(array(
            'name' => 'email',
            'required' => true,
            'validators' => array(
                array(
                    'name' => 'EmailAddress',
                    'options' => array(
                    ),
                ),
                array(
                    'name' => '\DoctrineModule\Validator\NoObjectExists',
                    'options' => array(
                        'object_repository' => $em->getRepository('Application\Entity\User'),
                        'fields' => array('email'),
                        'messages' => array(
                            \DoctrineModule\Validator\NoObjectExists::ERROR_OBJECT_FOUND    => "Email already exists in our Database",
                        ),
                    ),
                ),
            ),
            'filters' => array(
                array('name' => 'StripTags'),
                array('name' => 'StringTrim'),
            ),

        ));

        $this->add(array(
            'name' => 'password',
            'required' => true,
            'validators' => array(
                array(
                    'name' => 'StringLength',
                    'options' => array(
                        'min' => 6,
                    ),
                ),
            ),
            'filters' => array(
            ),

        ));

    }
}