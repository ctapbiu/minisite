<?php
namespace Application\Form;

use Zend\Form\Form;
use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;

class LogInForm extends Form {

  public function __construct($name = null) {
    parent::__construct('logIn');
    $this->setAttribute('method', 'post');
    $this->setInputFilter(new \Application\Form\LogInInputFilter());

    $this->add(array(
      'name' => 'email',
      'type' => 'Text',
      'options' => array(
        'label' => 'Email',
      ),
      'options' => array(
      ),
    ));

    $this->add(array(
      'name' => 'password',
      'type' => 'Password',
      'options' => array(
        'label' => 'Password',
      ),
      'options' => array(
      ),
    ));

    $this->add(array(
      'name' => 'submit',
      'type' => 'Submit',
    ));
  }
}