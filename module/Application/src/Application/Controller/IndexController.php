<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Application\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\Json\Json;
use Zend\Authentication\AuthenticationService;

use DoctrineORMModule\Paginator\Adapter\DoctrinePaginator;
use Doctrine\ORM\Tools\Pagination\Paginator as ORMPaginator;
use Zend\Paginator\Paginator;

class IndexController extends AbstractActionController {

  protected $loggedUser;
  protected $usersPerPage = 7;

  public function indexAction() {
    $authService = $this->getServiceLocator()->get('Zend\Authentication\AuthenticationService');
    $this->loggedUser = $authService->getIdentity();
    if (!$this->loggedUser) {
      return $this->redirect()->toRoute('signup');
    }

    $search = '';
    $searchForm = new \Application\Form\SearchForm();
    $searchForm->setData($this->params()->fromQuery());

    if ($searchForm->isValid()) {
      $searchData = $searchForm->getData();
      $search = $searchData['search'];
    }

    $em = $this->getServiceLocator()
      ->get('Doctrine\ORM\EntityManager');
    $repository = $em->getRepository('Application\Entity\User');

    $qb = $repository->createQueryBuilder('user');
    $qb->select('user')
      ->andWhere('user.id <> :id')
      ->orderBy('user.registered')
    ;
    if (0<strlen($search)) {
      $qb->andWhere(
        $qb->expr()->orx(
          'user.email LIKE :search',
          'user.name LIKE :search'
        )
      );
    }

    $dql = $qb->getDql();
    $query = $em->createQuery($dql)
      ->setParameter('id', $this->loggedUser->getId());
    if (0<strlen($search)) {
      //@todo: проверить экранирует ли ORM спецсимволы
      $query->setParameter('search', "%{$search}%");
    }

    $users = $query->getResult();


    $users = new Paginator(
      new DoctrinePaginator(new ORMPaginator($query))
    );

    $users
      ->setCurrentPageNumber((int)$this->params()->fromQuery('page', 1))
      ->setItemCountPerPage($this->usersPerPage)
      ->setPageRange(7)
    ;

    //=likes
    $likes = array();
    if (0<$users->count()) {
      $_users = array();
      foreach ($users as $u) {
        $_users[] = $u->getId();
      }

      $l_repository = $em->getRepository('Application\Entity\Like');

      $qb = $l_repository->createQueryBuilder('llike');
      $qb->select('llike')
        ->andWhere('llike.user_from_id = :id')
        ->andWhere($qb->expr()->in('llike.user_to_id', $_users))
      ;

      $dql = $qb->getDql();
      $query = $em->createQuery($dql)
        ->setParameter('id', $this->loggedUser->getId());

      $_likes = $query->getResult();
      if (0<count($_likes)) {
        foreach ($_likes as $l) {
          $likes[$l->getUser_to_id()->getId()] = 1;
        }
      }
    }
    //=!likes

    $paginatorView = new ViewModel(array('pages'=>$users->getPages(), 'search'=>$search));
    $paginatorView->setTemplate('application/index/paginator');

    $searchView = new ViewModel(array('searchForm'=>$searchForm));
    $searchView->setTemplate('application/index/search');

    $logInOutView = new ViewModel();
    $logInOutView->setTemplate('application/index/logout');
    $this->layout()->addChild($logInOutView, 'logInOut');

    $this->layout()->hasLogged = true;

    return new ViewModel(array(
      'users'=>$users,
      'paginator'=>$paginatorView,
      'search'=>$searchView,
      'likes'=>$likes,
      'remainLikes'=>$this->loggedUser->remainLikes($em),
    ));
  }

  public function signupAction() {
    $authService = $this->getServiceLocator()->get('Zend\Authentication\AuthenticationService');
    $this->loggedUser = $authService->getIdentity();
    if ($this->loggedUser) {
      return $this->redirect()->toRoute('home');
    }

    $logInOutView = new ViewModel(array('loginForm'=>new \Application\Form\LogInForm()));
    $logInOutView->setTemplate('application/index/login');
    $this->layout()->addChild($logInOutView, 'logInOut');

    $em = $this->getServiceLocator()
      ->get('Doctrine\ORM\EntityManager');
    $form = new \Application\Form\SignUpForm($em);

    $request = $this->getRequest();
    if ($request->isPost()) {
      $form->setData($request->getPost());

      if ($form->isValid()) {
        $objectManager = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');

        $user = new \Application\Entity\User();

        $user->setData($form->getData())
          ->setNum_likes(3)
        ;

        $objectManager->persist($user);
        $objectManager->flush();

        $message = 'User succesfully saved!';
        $this->flashMessenger()->addMessage($message);

        $this->loginAction();

//        return $this->redirect()->toRoute('home');
      }
      else {
        $message = 'Error while saving blogpost';
        $this->flashMessenger()->addErrorMessage($message);
      }
    }

    $this->layout()->bodyClass = 'signup';

    return array('form' => $form);
  }

  public function loginAction() {
    $form = new \Application\Form\LogInForm();

    $request = $this->getRequest();
    if ($request->isPost()) {
      $form->setData($request->getPost());

      if ($form->isValid()) {
        $data = $this->getRequest()->getPost();

        $authService = $this->getServiceLocator()->get('Zend\Authentication\AuthenticationService');

        $adapter = $authService->getAdapter();
        $adapter->setIdentityValue($data['email']);
        $adapter->setCredentialValue($data['password']);
        $authResult = $authService->authenticate();

        if ($authResult->isValid()) {
          $identity = $authResult->getIdentity();
          return $this->redirect()->toRoute('home');
        }
        else {
          return $this->redirect()->toRoute('signup');
        }
      }
    }

    return $this->redirect()->toRoute('signup');
  }


  public function logoutAction() {
    $authService = $this->getServiceLocator()->get('Zend\Authentication\AuthenticationService');
    $authService->clearIdentity();

    return $this->redirect()->toRoute('home');
  }

  public function likeAction() {
    $authService = $this->getServiceLocator()->get('Zend\Authentication\AuthenticationService');
    $this->loggedUser = $authService->getIdentity();

    $translator = $this->getServiceLocator()->get('translator');

    if (!$this->loggedUser) {
      $msg = $translator->translate('Только залогиненный пользователь может лайкать');
      return $this->getResponse()->setContent(Json::encode(array('error'=>'notLogged', 'msg'=>$msg)));
    }

    $request = $this->getRequest();

    if ($request->isXmlHttpRequest()) {
      $data = $request->getPost();

      if ($data['id']==$this->loggedUser->getId()) {
        $msg = $translator->translate('Нельзя лайкать самого себя');
        return $this->getResponse()->setContent(Json::encode(array('error'=>'himselfLiked', 'msg'=>$msg)));
      }

      $em = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');

      $lUser = $em->find('Application\Entity\User', $data['id']);
      if (!$lUser) {
        $msg = $translator->translate('Пользователя не существует');
        return $this->getResponse()->setContent(Json::encode(array('error'=>'userNotExist', 'msg'=>$msg)));
      }

      $status = trim(strtolower($data['status']));
      if (!in_array($status, array('not-liked', 'liked'))) {
        $status = 'trigger';
      }

      $like = $this->loggedUser->hasLiked($em, $lUser);

      if ('trigger'==$status) {
        if ($like) {
          $status = 'liked';
        }
        else {
          $status = 'not-liked';
        }
      }

      if ('not-liked'==$status) {
        if ($like) {
          $msg = $translator->translate('Этого пользователя вы уже лайкали');
          return $this->getResponse()->setContent(Json::encode(array('error'=>'userAlreadyLiked', 'msg'=>$msg)));
        }

        if (0>=$this->loggedUser->remainLikes($em)) {
          $msg = $translator->translate('У вас больше нет лайков');
          return $this->getResponse()->setContent(Json::encode(array('error'=>'noMoreLikes', 'msg'=>$msg)));
        }

        $like = new \Application\Entity\Like();
        $like->setUser_from_id($this->loggedUser);
        $like->setUser_to_id($lUser);
        $em->persist($like);
        $em->flush();
        $msg = $translator->translate('Лайк поставлен');
        return $this->getResponse()->setContent(Json::encode(array('success'=>'userLiked', 'msg'=>$msg, 'remainLikes'=>$this->loggedUser->remainLikes($em))));
      }
      elseif ('liked'==$status) {
        if (!$like) {
          $msg = $translator->translate('Невозможно отменить лайк, потому что пользователь и так не был лайкнут');
          return $this->getResponse()->setContent(Json::encode(array('error'=>'userNotLiked', 'msg'=>$msg)));
        }

        $em->remove($like);
        $em->flush();
        $msg = $translator->translate('Лайк убран');
        return $this->getResponse()->setContent(Json::encode(array('success'=>'userUnLiked', 'msg'=>$msg, 'remainLikes'=>$this->loggedUser->remainLikes($em))));
      }

      $msg = $translator->translate('Обманывать не хорошо!');
      return $this->getResponse()->setContent(Json::encode(array('error'=>'notSelectedStatus', 'msg'=>$msg)));
    }

    $msg = $translator->translate('Обманывать не хорошо!');
    return $this->getResponse()->setContent(Json::encode(array('error'=>'notXmlHttpRequest', 'msg'=>$msg)));
  }


}
