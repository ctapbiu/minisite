<?php
namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

use Doctrine\ORM\EntityManager;

/**
 * @ORM\Entity
 * @ORM\Table(name="users")
 */
class User {
  /**
  * @ORM\Id
  * @ORM\Column(type="integer")
  * @ORM\GeneratedValue(strategy="AUTO")
  */
  protected $id;

  /**
   * @ORM\Column(type="string", length=255, nullable=false)
   */
  protected $name;

  /**
   * @ORM\Column(type="string", length=255, nullable=false, unique=true)
   */
  protected $email;

  /**
   * @ORM\Column(type="string", length=64, nullable=false)
   */
  protected $password;

  /**
   * @ORM\Column(type="integer", nullable=false)
   */
  protected $num_likes;

  /**
   * @ORM\Column(type="datetime")
   * @ORM\Version
   */
  protected $registered;

  //=getters/setters
  public function getId() {
    return $this->id;
  }

  public function setId($id) {
    $this->id = (int) $id;
    return $this;
  }

  public function getName() {
    return $this->name;
  }

  public function setName($name) {
    $this->name = $name;
    return $this;
  }

  public function getEmail() {
    return $this->email;
  }

  public function setEmail($email) {
    $this->email = $email;
    return $this;
  }

  public function getPassword() {
    return $this->password;
  }

  public function setPassword($password) {
    $this->password = md5($password);
    return $this;
  }

  public function getNum_likes() {
    return $this->num_likes;
  }

  public function setNum_likes($num_likes) {
    $this->num_likes = (int) $num_likes;
    return $this;
  }

  public function getRegistered() {
    return $this->registered;
  }

  public function setRegistered($registered) {
    $this->registered = $registered;
    return $this;
  }

  //=!getters/setters


  public function setData($data) {
    foreach ($data as $key => $val) {
      if (property_exists($this, $key)) {
        if ('password'==$key) {
          $val = md5($val);
        }
        $this->$key = (!empty($val)) ? $val : null;
      }
    }
    return $this;
  }

  public function remainLikes(EntityManager $em) {
    $rep = $em->getRepository('Application\Entity\Like');

    $qb = $rep->createQueryBuilder('llike');
    $qb->select('count(llike.id)')
      ->andWhere('llike.user_from_id = :id')
    ;

    $dql = $qb->getDql();
    $query = $em->createQuery($dql)
      ->setParameter('id', $this->id);
    $numLikes = $query->getSingleScalarResult();

    return $this->num_likes - $numLikes;
  }

  public function hasLiked(EntityManager $em, User $user) {
    $rep = $em->getRepository('Application\Entity\Like');

    $qb = $rep->createQueryBuilder('llike');
    $qb->select('llike')
      ->andWhere('llike.user_from_id = :fromId')
      ->andWhere('llike.user_to_id = :toId')
    ;

    $dql = $qb->getDql();
    $query = $em->createQuery($dql)
      ->setParameter('fromId', $this->id)
      ->setParameter('toId', $user->getId())
    ;

    $like = $query->getOneOrNullResult();

    return $like;
  }


}