<?php
namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="likes")
 */
class Like {
  /**
  * @ORM\Id
  * @ORM\Column(type="integer")
  * @ORM\GeneratedValue(strategy="AUTO")
  */
  protected $id;

  /**
   * @ ORM\Colu111mn(type="integer", nullable=false)
   * @ORM\ManyToOne(targetEntity="Application\Entity\User")
   * @ORM\JoinColumns({
   *   @ORM\JoinColumn(name="user_from_id", referencedColumnName="id")
   * })
   */
  protected $user_from_id;

  /**
   * @ ORM\Column(type="integer", nullable=false)
   * @ORM\ManyToOne(targetEntity="Application\Entity\User")
   * @ORM\JoinColumns({
   *   @ORM\JoinColumn(name="user_to_id", referencedColumnName="id")
   * })
   */
  protected $user_to_id;

  /**
   * @ORM\Column(type="datetime")
   * @ORM\Version
   */
  protected $like_datetime;

  //=getters/setters
  public function getId() {
    return $this->id;
  }

  public function setId($id) {
    $this->id = (int) $id;
    return $this;
  }

  public function getUser_from_id() {
    return $this->user_from_id;
  }

  public function setUser_from_id(\Application\Entity\User $user_from=null) {
    $this->user_from_id = $user_from;
    return $this;
  }

  public function getUser_to_id() {
    return $this->user_to_id;
  }

  public function setUser_to_id(\Application\Entity\User $user_to=null) {
    $this->user_to_id = $user_to;
    return $this;
  }

  public function getLike_datetime() {
    return $this->like_datetime;
  }

  public function setLike_datetime($like_datetime) {
    $this->like_datetime = $like_datetime;
    return $this;
  }

  //=!getters/setters


  public function setData($data) {
    foreach ($data as $key => $val) {
      if (property_exists($this, $key)) {
        $this->$key = (!empty($val)) ? $val : null;
      }
    }
    return $this;
  }


}